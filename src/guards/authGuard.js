import firebase from 'boot/firebase';

const requireAuth = (to, from, next) => {
  const user = firebase.auth().currentUser;
  if (user) {
    // User is authenticated, proceed to the route
    next();
  } else {
    // User is not authenticated, redirect to login
    next('/login');
  }
};

export default requireAuth;
