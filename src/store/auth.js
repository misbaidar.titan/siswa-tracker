import firebase from 'boot/firebase';

const state = {
  user: null,
};

const mutations = {
  setUser(state, payload) {
    state.user = payload;
  },
};

const actions = {
  async login({ commit }, { email, password }) {
    try {
      const { user } = await firebase.auth().signInWithEmailAndPassword(email, password);
      commit('setUser', user);
      this.$router.push('/dashboard');
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  async register({ commit }, { email, password }) {
    try {
      const { user } = await firebase.auth().createUserWithEmailAndPassword(email, password);
      commit('setUser', user);
      this.$router.push('/login');
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  async logout({ commit }) {
    try {
      await firebase.auth().signOut();
      commit('setUser', null);
      this.$router.push('/auth');
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};

const getters = {
  isAuthenticated(state) {
    return !!state.user;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
