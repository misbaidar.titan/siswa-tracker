import Login from '../components/Auth/Login.vue'
import Register from '../components/Auth/Register.vue'
import requireAuth from '../guards/authGuard.js';

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'), beforeEnter: requireAuth,
    children: [
      { path: '/dashboard', component: () => import('pages/IndexPage.vue') },
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/AuthPage.vue'),
  },
  { path: '/login', component: Login },
  { path: '/register', component: Register },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
