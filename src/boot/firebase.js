import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAJuVQHJsCoIzbGyqyuNfF3e1TySP9_Mjg",
  authDomain: "siswa-tracker-c7422.firebaseapp.com",
  projectId: "siswa-tracker-c7422",
  storageBucket: "siswa-tracker-c7422.appspot.com",
  messagingSenderId: "506061020609",
  appId: "1:506061020609:web:52a1426652c5e7b4842340"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
